$(document).ready(function () {
  let total = 0,
    check_count = 0;
  let todos = [];

  //데이터 업데이트 및 localStorage에 저장
  function updateText() {
    $('#count').text(total);
    $('#count_done').text(check_count);
    $('#remaining_done').text(total - check_count);

    saveTodosToLocalStorage(); // Save to localStorage
  }

  //app-header__date update
  function showDate() {
    let date = new Date();
    let dayOfMonth = date.getDate();
    let dayOfWeek = date.getDay();
    let Month = date.getMonth();

    let $today = $('#today');
    let $daymonth = $('#daymonth');
    let $month = $('#month');

    const dayArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const monthArray = ['January', 'February', 'March', 'April,', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    const suffixes = ['th', 'st', 'nd', 'rd'];
    let suffixeIndex = dayOfMonth % 10 == 0 || dayOfMonth % 10 >= 4 ? 0 : dayOfMonth % 10;
    let suffix = suffixes[suffixeIndex];

    $today.text(dayArray[dayOfWeek] + ',');
    $daymonth.text(' ' + dayOfMonth + suffix);
    $month.text(monthArray[Month]);
  }

  //Load todos from localStorage
  function loadTodosFromLocalStorage() {
    let storedTodos = localStorage.getItem('todos');
    if (storedTodos) {
      todos = JSON.parse(storedTodos); //JSON 문자열을 객체나 배열로 변환하는 메소드
      total = todos.length;

      todos.forEach(todo => {
        let isChecked = 'far fa-circle mark-alt';
        let contentClass = '';
        let contentEditable = true;
        let todoContentDone = '';
        if (todo.checked) {
          isChecked = 'fa fa-check-circle mark';
          contentClass = 'todo__content--done';
          contentEditable = false;
          todoContentDone = 'todo__content--done';
          check_count++;
        }

        let load_li = `<li class="todo"><button href="" class="todo__check_button" onmousedown="return false"><i class="${isChecked} check_icon" aria-hidden="true"></i></button><div class="todo__content"><p contenteditable="${contentEditable}" class="${todoContentDone}"><strong>${todo.content}</strong></p></div><button class="todo__delete_button" onmousedown="return false"><i class="fa fa-times-circle delete_icon" aria-hidden="true"></i></button></li>`;

        $('.app-main__container').append(load_li);
      });
    }
    updateText();
  }

  //Save todos to localStorage
  function saveTodosToLocalStorage() {
    localStorage.setItem('todos', JSON.stringify(todos));
  }

  loadTodosFromLocalStorage(); //localStorage의 데이터 불러오기

  //시작시 애니메이션
  $('.add_todo').addClass('show');
  $('.app-main__container .todo').each(function (j) {
    let $this = $(this);
    setTimeout(function () {
      $this.addClass('down_in').removeClass('todohidden');
      setTimeout(function () {
        $this.removeAttr('class').addClass('todo');
      }, 550);
    }, 60 * j);
  });

  //Click button을 선택하여 todo 추가
  $('.add_todo #add-new').click(function (e) {
    e.preventDefault();

    let newTodoContent = 'New Task';
    todos.push({ content: newTodoContent, checked: false });
    let created_li = `<li class="todo"><button href="" class="todo__check_button" onmousedown="return false"><i class="far fa-circle check_icon" aria-hidden="true"></i></button><div class="todo__content"><p contenteditable="true"><strong>${newTodoContent}</strong></p></div><button class="todo__delete_button" onmousedown="return false"><i class="fa fa-times-circle delete_icon" aria-hidden="true"></i></button></li>`;

    $('.app-main__container').append(created_li).find('li:last-child').addClass('down');

    total += 1;
    updateText();
  });

  // Click on button function list - todo check 버튼 클릭 시 이벤트
  $('.app .app-main__container').on('click', '.todo .todo__check_button', function (e) {
    e.preventDefault();

    let button = $(this).find('i');
    let checked = 'fa fa-check-circle mark';
    let unchecked = 'far fa-circle';

    // Save the current index of button after the click event in the "left" div.
    let index_click = $('.todo .todo__check_button').index(this);
    // Use the current index of button to target the correct "li p" in the "right" div.
    let linethrough_text = $('.todo .todo__content').eq(index_click).find('p');

    if (button.hasClass(unchecked)) {
      linethrough_text.addClass('todo__content--done').attr('contentEditable', false);
      button
        .removeClass(unchecked + ' mark-alt')
        .addClass('pop_in')
        .addClass(checked);
      todos[index_click].checked = true;
      check_count++;
    } else {
      linethrough_text.removeClass('todo__content--done').attr('contentEditable', true);
      button
        .removeClass(checked)
        .removeClass('pop_in')
        .addClass(unchecked + ' mark-alt');
      todos[index_click].checked = false;
      check_count--;
    }

    updateText();
  });

  //todo content 변경 시 이벤트
  $('.app .app-main__container').on('input', '.todo .todo__content p', function (e) {
    let index_click = $('.todo .todo__content p').index(this);
    todos[index_click].content = $(this).text();

    updateText();
  });

  // Click on button function and delete 'li' - delete버튼 클릭 시 이벤트
  $('.app .app-main__container').on('click', '.todo .todo__delete_button', function (e) {
    e.preventDefault();
    let index_click = $('.todo .todo__delete_button').index(this);
    todos.splice(index_click, 1);

    let current = $('.todo').eq(index_click);
    let button = $('.todo__check_button').find('i');

    $(this).prop('disabled', true);
    total--;

    if (button.eq(index_click).hasClass('mark')) {
      check_count--;
    }

    current.addClass('up');
    setTimeout(function () {
      current.remove();
    }, 560);

    updateText();
  });

  showDate();
});
