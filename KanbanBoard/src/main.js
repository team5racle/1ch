const ContainerColor = {
  Todo: '#7e57c2',
  Task: '#5c6bc0',
  Done: '#66bb6a',
  Idea: '#7e57c2',
  Bug: '#ef5350',
  Dev: '#ffa726',
  Complete: '#66bb6a',
};

const tagTypes = ['Todo', 'Task', 'Done'];

$(document).ready(function () {
  //로컬 스토리에 저장된 데이터 불러오기
  function loadKanbanDatasFromLocalStorage() {
    const storedKanbanData = localStorage.getItem('kanbanDatas');
    if (storedKanbanData) {
      kanbanDatas = JSON.parse(storedKanbanData);

      // 각 태그 별 가장 첫번째 요소만 찾기
      const firstElements = {};
      $.each(kanbanDatas, function (key, value) {
        if (value.prevId === -1) firstElements[key] = value;
      });

      //jQuery에서 사용하는 일반적인 반복함수 (object와 배열 모두 사용 가능)
      $.each(firstElements, function (key, value) {
        let id = key;
        do {
          let tag = kanbanDatas[id]['tag'];
          const loadItem = $(
            `<div class="draggable" draggable="true" id="${id}"><span class="tag" draggable="false" style="background-color: ${ContainerColor[tag]}">${tag}</span><p draggable="false" contenteditable="true">${kanbanDatas[id]['content']}</p><button class="delete-button" draggable="false">🗑️</button></div>`
          );

          if (max_id < id) max_id = id;
          // .board__container 클래스를 갖는 태그 중 해당 타입 갖는 태그들만 찾기
          $(`.board__container[data-type=${tag}]`).children('.container').append(loadItem);

          id = kanbanDatas[id]['nextId'];
        } while (id !== -1);
      });
    }
  }

  //로컬 스토리지에 저장
  function saveKanbanDatasToLocalStorage() {
    localStorage.setItem('kanbanDatas', JSON.stringify(kanbanDatas));
  }

  /////////////////////////////////////////////////////////////////////////
  //드래그 이벤트
  function dragstart(e) {
    if (e.target.isContentEditable) return;
    const draggable = $(e.target);
    // draggable.children('p').attr('contenteditable', 'false');
    draggable.addClass('dragging');
  }

  function dragend(e) {
    const draggable = $(e.target);

    const dataType = draggable.parent().parent().data('type');
    const tag = draggable.children('.tag');

    tag.html(dataType);
    tag.css('background-color', ContainerColor[dataType]);
    draggable.removeClass('dragging');

    //localStorage 저장
    const id = draggable.attr('id');
    kanbanDatas[id]['tag'] = dataType;

    //이동 전 연결된 Item들 최신화
    const beforePrevId = kanbanDatas[id]['prevId'];
    const beforeNextId = kanbanDatas[id]['nextId'];
    if (beforePrevId !== -1) {
      kanbanDatas[beforePrevId]['nextId'] = beforeNextId;
    }
    if (beforeNextId !== -1) {
      kanbanDatas[beforeNextId]['prevId'] = beforePrevId;
    }

    //이동 후 연결된 Item들 최신화
    const prevItem = draggable.prev();
    const nextItem = draggable.next();
    let prevItemId = -1;
    let nextItemId = -1;

    if (prevItem.length !== 0) {
      prevItemId = prevItem.attr('id');
      kanbanDatas[prevItemId]['nextId'] = id;
    }
    kanbanDatas[id]['prevId'] = prevItemId;

    if (nextItem.length !== 0) {
      nextItemId = nextItem.attr('id');
      kanbanDatas[nextItemId]['prevId'] = id;
    }
    kanbanDatas[id]['nextId'] = nextItemId;

    saveKanbanDatasToLocalStorage();
  }

  function getDragAfterElement(container, y) {
    const draggableElements = container.find('.draggable:not(.dragging)');
    return draggableElements.toArray().reduce(
      (closest, child) => {
        const box = $(child)[0].getBoundingClientRect();
        const offset = y - box.top - box.height / 2;
        if (offset < 0 && offset > closest.offset) {
          return { offset: offset, element: child };
        } else {
          return closest;
        }
      },
      { offset: Number.NEGATIVE_INFINITY }
    ).element;
  }

  /////////////////////////////////////////////////////////////////////////
  ////delete-button 클릭 시 해당 Item 제거 이벤트
  function deleteButton(e) {
    const draggable = $(e.target).parent();

    //kanbanDatas 최신화
    const k_id = draggable.attr('id');
    const prevId = kanbanDatas[k_id]['prevId'];
    const nextId = kanbanDatas[k_id]['nextId'];

    if (prevId !== -1) kanbanDatas[prevId]['nextId'] = nextId; //이전 아이템최신화
    if (nextId !== -1) kanbanDatas[nextId]['prevId'] = prevId; //다음 아이템 최신화
    delete kanbanDatas[k_id];

    draggable.remove();
    saveKanbanDatasToLocalStorage();
  }

  //Item input 내용 변경 시 localStorage 저장 이벤트
  function saveInputText(e) {
    let k_id = $(e.target).parent().attr('id');
    kanbanDatas[k_id].content = $(e.target).text();

    saveKanbanDatasToLocalStorage();
  }

  //localStorage 데이터
  let kanbanDatas = {};
  let max_id = 0; //샘플 데이터 개수
  /*
    kanbanDatas = {
      1 : {
        tag: 'Task',
        content: 'ID가 있어야 변경, 수정, 삭제에 용이',
        prevId: -1, 이전 블록 아이템 ID, -1이면 가장 위
        nextId: -1, 다음 블록 아이템 ID, 가장 아래
      }
      ,...
    }
  */
  loadKanbanDatasFromLocalStorage(); //local storage에서 데이터 불러오기
  $('.draggable').on('dragstart', dragstart);
  $('.draggable').on('dragend', dragend);

  $('.container').on('dragover', function (e) {
    e.preventDefault();
    const afterElement = getDragAfterElement($(this), e.clientY);
    const draggable = $('.dragging')[0];
    if (afterElement === undefined) {
      $(this).append(draggable);
    } else {
      $(draggable).insertBefore(afterElement);
    }
  });

  $('.board__container').each(function () {
    const boardContainer = $(this);
    const dataType = boardContainer.data('type');

    //add-button 클릭 시 새로운 Item 추가 이벤트
    $('.add-button', boardContainer).on('click', function (e) {
      const content = `${dataType}: New ${dataType}`;
      const prevId = boardContainer.children('.container').children().last().attr('id');
      const newItem = $(
        `<div class="draggable" draggable="true" id="${++max_id}">
          <span class="tag" draggable="false" style="background-color: ${
            ContainerColor[dataType]
          }">${dataType}</span><p draggable="false" contenteditable="true">${content}</p><button class="delete-button" draggable="false">🗑️</button></div>`
      );

      //새로 생성된 Item에 필요한 이벤트 적용
      newItem.on('dragstart', dragstart);
      newItem.on('dragend', dragend);
      newItem.children('p').on('input', saveInputText);
      $('.delete-button', newItem).on('click', deleteButton);

      $('.container', boardContainer).append(newItem);

      kanbanDatas[max_id] = {
        tag: dataType,
        content: content,
        prevId: prevId == undefined ? -1 : prevId,
        nextId: -1,
      };

      if (prevId !== undefined) {
        kanbanDatas[prevId]['nextId'] = max_id; //kanbanDatas 수정
      }
      saveKanbanDatasToLocalStorage();
    });

    //delete-button 클릭 시 해당 Item 제거 이벤트 등록
    $('.delete-button', boardContainer).on('click', deleteButton);
  });

  //Item input 내용 변경 시 localStorage 저장 이벤트 등록
  $('.draggable').children('p').on('input', saveInputText);
});
