const create = document.getElementById("add");
const firstList = document.getElementById("create");

create.addEventListener("click", () => {
    const inputDiv = document.createElement("div");
    const input = document.createElement("input");
    const deleteBtn = document.createElement("button");

    deleteBtn.classList.add("delete");
    deleteBtn.innerText = "삭제";

    firstList.appendChild(inputDiv);
    inputDiv.setAttribute("id", "draggable");
    inputDiv.setAttribute("draggable", "true");
    inputDiv.appendChild(input);
    inputDiv.appendChild(deleteBtn);

    deleteBtn.addEventListener("click", () => {
        inputDiv.remove();
    });
});

let dragged;  // 드래그 선언

// 드래그 이벤트 로그 찍어보기
document.addEventListener("drag", (e) => {  // 100밀리초마다 발생
    console.log("dragging")
});

// 드래그 이벤트 시작
document.addEventListener("dragstart", (e) => {
    dragged = e.target;  // 드래그 요소에 대한 참조 저장
    e.target.classList.add("dragging");  // 반투명
});



// 드래그한 요소가 유효한 드롭대상에 들어갈 때
document.addEventListener("dragenter", (e) => {
    if (e.target.classList.contains("dropzone"))
        e.target.classList.add("dragover");
});

// 드래그한 요소가 유효한 드롭대상 위로 드래그 될 때
document.addEventListener("dragover", (e) => {
    e.preventDefault();  // 드래그가 끝나고 드롭을 하기 위해 드래그 이벤트 종료
    }, false
);

// 드래그한 요소가 드롭 대상을 벗어낫을 때
document.addEventListener("dragleave", (e) => {
    if (e.target.classList.contains("dropzone"))
        e.target.classList.remove("dragover");
});

// 드래그한 요소를 드롭햇을 때
document.addEventListener("drop", (e) => {
    e.preventDefault();

    if (e.target.classList.contains("dropzone")) {
        e.target.classList.remove("dragover");
        dragged.parentNode.removeChild(dragged);
        e.target.appendChild(dragged);
    }
});

// 드래그 이벤트 종료
document.addEventListener("dragend", (e) => {
    e.target.classList.remove("dragging");  // 투명도 초기화
});
