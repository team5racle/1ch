let radius = 240; // 회전반경
let autoRotate = true; // 자동 회전
let rotateSpeed = -60; // 회전 속도
let imgWidth = 300; // width of images (unit: px)
let imgHeight = 170; // height of images (unit: px)

let bgMusicURL =
    "https://api.soundcloud.com/tracks/143041228/stream?client_id=587aa2d384f7333a886010d5f52f302a";
let bgMusicControls = true;

setTimeout(init, 1000); // 1초 후 시작

let odrag = document.getElementById("drag-container");
let ospin = document.getElementById("spin-container");
let aImg = ospin.getElementsByTagName("img");
let aVid = ospin.getElementsByTagName("video");
let aEle = [...aImg, ...aVid]; // 이미지와 비디오 저장

// 이미지 사이즈
ospin.style.width = imgWidth + "px";
ospin.style.height = imgHeight + "px";

// 바닥 사이즈
let ground = document.getElementById("ground");
ground.style.width = radius * 3 + "px";
ground.style.height = radius * 3 + "px";

function init(delayTime) {
    for (let i = 0; i < aEle.length; i++) {
        aEle[i].style.transform =
            "rotateY(" +
            i * (360 / aEle.length) +
            "deg) translateZ(" +
            radius +
            "px)";
        aEle[i].style.transition = "transform 1s";
        aEle[i].style.transitionDelay =
            delayTime || (aEle.length - i) / 4 + "s";
    }
}

function applyTranform(obj) {
    if (tY > 180) tY = 180;
    if (tY < 0) tY = 0;
    obj.style.transform = "rotateX(" + -tY + "deg) rotateY(" + tX + "deg)";
}

function playSpin(yes) {
    ospin.style.animationPlayState = yes ? "running" : "paused";
}

let sX,
    sY,
    nX,
    nY,
    desX = 0,
    desY = 0,
    tX = 0,
    tY = 10;

// 자동 회전
if (autoRotate) {
    let animationName = rotateSpeed > 0 ? "spin" : "spinRevert";
    ospin.style.animation = `${animationName} ${Math.abs(
        rotateSpeed
    )}s infinite linear`;
}

if (bgMusicURL) {
    document.getElementById("music-container").innerHTML += `
<audio src="${bgMusicURL}" ${
        bgMusicControls ? "controls" : ""
    } autoplay loop>    
<p>If you are reading this, it is because your browser does not support the audio element.</p>
</audio>
`;
}

// 이벤트
document.onpointerdown = function (e) {
    clearInterval(odrag.timer);
    e = e || window.e;
    let sX = e.clientX,
        sY = e.clientY;

    this.onpointermove = function (e) {
        e = e || window.e;
        let nX = e.clientX,
            nY = e.clientY;
        desX = nX - sX;
        desY = nY - sY;
        tX += desX * 0.1;
        tY += desY * 0.1;
        applyTranform(odrag);
        sX = nX;
        sY = nY;
    };

    this.onpointerup = function (e) {
        odrag.timer = setInterval(function () {
            desX *= 0.95;
            desY *= 0.95;
            tX += desX * 0.1;
            tY += desY * 0.1;
            applyTranform(odrag);
            playSpin(false);
            if (Math.abs(desX) < 0.5 && Math.abs(desY) < 0.5) {
                clearInterval(odrag.timer);
                playSpin(true);
            }
        }, 17);
        this.onpointermove = this.onpointerup = null;
    };

    return false;
};

document.onmousewheel = function (e) {
    e = e || window.e;
    let d = e.wheelDelta / 20 || -e.detail;
    radius += d;
    init(1);
};

// 비디오 목록 조회
let videoList = document.querySelectorAll("#spin-container video");

// 비디오에 인덱스 할당
videoList.forEach((video, index) => {
    video.setAttribute("video-index", index);
});

// 각 비디오에 대해 이름을 목록에 추가
videoList.forEach((video, index) => {
    const videoName = video.getAttribute("title");
    video.setAttribute("video-index", index); // video-index 설정
    document.getElementById(
        "video-list"
    ).innerHTML += `<li video-index="${index}">${videoName}</li>`; // li 요소에 video-index도 추가
});

// 모달을 위해 가져온 src
let videoSrc = null;

// 파일 추가 이벤트핸들러
function handlerAddFile() {
    const files = this.files;

    // 기존 비디오와 새로운 비디오를 합친 비디오 개수
    const totalVideosCount =
        document.querySelectorAll("#spin-container video").length +
        files.length;

    // 새로운 비디오를 추가할 때마다 회전시킬 인덱스 증가 변수
    let spinIndex = document.querySelectorAll("#spin-container video").length;

    // 새로운 비디오를 추가할 때마다 인덱스 증가 변수
    let newIndex = document.querySelectorAll("#spin-container video").length;
    // 360도를 모든 비디오들로 나누어서 각도 간격을 계산
    const angleInterval = 360 / totalVideosCount;

    // 모든 비디오들을 일정한 간격으로 배치
    document
        .querySelectorAll("#spin-container video")
        .forEach((video, index) => {
            const angle = index * angleInterval;
            video.style.transform = `rotateY(${angle}deg) translateZ(${radius}px)`;
            video.style.transitionDelay = `${(totalVideosCount - index) / 4}s`;
        });

    // 파일 리더 이용
    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const reader = new FileReader();

        reader.onload = (e) => {
            const fileType = file.type.split("/")[0];
            if (fileType === "video") {
                // 비디오만 허용
                const mediaElement = document.createElement(fileType);
                mediaElement.src = e.target.result;
                //mediaElement.controls = true;
                mediaElement.width = imgWidth;
                mediaElement.height = imgHeight;

                newIndex += plusIndex; // 인덱스 증가

                // 새로 추가된 비디오를 올바른 각도로 배치
                const angle = spinIndex * angleInterval;
                mediaElement.style.transform = `rotateY(${angle}deg) translateZ(${radius}px)`;
                mediaElement.style.transitionDelay = `${
                    (totalVideosCount - spinIndex) / 4
                }s`;
                mediaElement.setAttribute("video-index", newIndex);
                mediaElement.setAttribute("class", "video");
                ospin.appendChild(mediaElement);

                // 비디오 이름을 가져와서 목록에 추가하고 video-index 설정
                const videoName = file.name.replace(/\.[^/.]+$/, ""); // 확장자를 제외한 파일 이름만 가져오기
                document.getElementById(
                    "video-list"
                ).innerHTML += `<li video-index="${newIndex}">${videoName}</li>`;
                addClickEventToVideoItems(); // 새로운 비디오에 클릭 이벤트 추가

                // 모달을 위해 가져온 src
                videoSrc = e.target.result; // FileReader 결과를 videoSrc에 할당
            } else {
                alert("비디오만 업로드할 수 있습니다.");
            }
        };
        reader.readAsDataURL(file);
    }
}

document.getElementById("add-file").addEventListener("change", handlerAddFile);

// 비디오 목록 항목에 클릭 이벤트 추가 함수
// 재사용(2번) 사용하므로 따로 함수로 선언
const addClickEventToVideoItems = () => {
    const videoItems = document.querySelectorAll("#video-list li");
    videoItems.forEach((item) => {
        item.addEventListener("click", function () {
            videoItems.forEach((item) => item.classList.remove("clicked")); // 모든 비디오의 clicked 클래스 제거
            this.classList.add("clicked"); // 클릭된 비디오에 clicked 클래스 추가

            // 선택된 비디오의 인덱스 가져오기
            const videoIndex = parseInt(this.getAttribute("video-index"));

            // spin-container에서 선택된 비디오 찾기
            const selectedVideo = document.querySelector(
                `#spin-container video[video-index="${videoIndex}"]`
            );

            // 모달을 위해 가져온 src
            // const sourceElement = selectedVideo.querySelector('source');
            // if (sourceElement) {
            //     console.log(sourceElement)
            //     videoSrc = sourceElement.getAttribute('src');
            //     console.log("선택된 비디오의 소스(src):", videoSrc);
            // } else {
            //     console.error("비디오 소스를 찾을 수 없습니다.");
            // }

            // 모달을 위해 가져온 src
            videoSrc = selectedVideo.getAttribute("src");

            // spin-container에서 이전에 선택된 모든 비디오의 'selected' 클래스 제거
            const allVideos = document.querySelectorAll(
                "#spin-container video"
            );
            allVideos.forEach((video) => video.classList.remove("selected"));

            // 선택된 비디오에 'selected' 클래스 추가
            selectedVideo.classList.add("selected");
        });
    });
};

addClickEventToVideoItems();

// 비디오 추가에 사용할 인덱스
let plusIndex = 0;

// 파일 삭제 함수
const removeVideo = (videoIndex) => {
    const videoToRemove = document.querySelector(
        `#spin-container video[video-index="${videoIndex}"]`
    );

    if (videoToRemove) {
        videoToRemove.remove();
    }

    // 비디오 삭제 후 재배치
    const videos = document.querySelectorAll("#spin-container video");
    videos.forEach((video, index) => {
        const angle = index * (360 / videos.length);
        video.style.transform = `rotateY(${angle}deg) translateZ(${radius}px)`;
        video.style.transitionDelay = `${(videos.length - index) / 4}s`;
    });
};

// 삭제 버튼 클릭 이벤트
document.getElementById("remove-file").addEventListener("click", () => {
    // 선택된 비디오 요소 가져오기
    const selectedVideo = document.querySelector("#video-list li.clicked");

    const allVideos = document.querySelectorAll("#spin-container video");

    if (selectedVideo) {
        // 선택된 비디오가 있다면

        // 삭제 확인
        const confirmDelete = confirm("정말 삭제하시겠습니까?");
        if (confirmDelete) {
            // 선택된 비디오의 인덱스 가져오기
            const videoIndex = parseInt(
                selectedVideo.getAttribute("video-index")
            );

            //console.log("현재 파일 인덱스: " + videoIndex)

            // 선택된 비디오 제거
            selectedVideo.remove();

            // 비디오 목록에서 제거
            removeVideo(videoIndex);

            plusIndex++; // 비디오 등록에 사용할 인덱스 증가
        } else {
            allVideos.forEach((video) => video.classList.remove("selected"));
            selectedVideo.classList.remove("clicked");
        }
    } else {
        alert("삭제할 비디오를 선택하세요.");
    }
});

// 모달에 재생
document.getElementById("view-video").addEventListener("click", () => {
    const selectedVideo = document.querySelector("#video-list li.clicked");

    if (selectedVideo) {
        const videoIndex = parseInt(selectedVideo.getAttribute("video-index"));
        const videoToPlay = document.querySelector(
            `#spin-container video[video-index="${videoIndex}"]`
        );
        let modal = document.getElementById("video-modal"); // const 안됨
        let modalVideo = document.getElementById("modal-video"); // const 안됨

        // 비디오 요소의 모든 자식 요소 확인
        const children = videoToPlay.childNodes;
        children.forEach((child) => {
            // 자식 요소 중에서 소스 요소인 경우 src 속성 가져오기
            if (child.nodeName === "SOURCE") {
                videoSrc = child.getAttribute("src");
            }
        });

        if (videoSrc) {
            // 비디오 소스가 있는 경우
            modal.style.display = "block";
            modalVideo = document.getElementById("modal-video");
            const modalSource = document.getElementById("modal-source");

            // 모달 비디오의 소스(src)를 선택된 비디오의 소스(src)로 설정
            modalSource.setAttribute("src", videoSrc);
            modalVideo.load();
            modalVideo.play();
            console.log("재생성공");
        } else {
            console.error("비디오 소스를 찾을 수 없습니다.");
        }
    } else {
        alert("재생할 비디오를 선택하세요.");
    }
});

// 모달 창 닫기 버튼 이벤트 처리
document
    .getElementsByClassName("modal-close")[0]
    .addEventListener("click", () => {
        const modal = document.getElementById("video-modal");
        const modalVideo = document.getElementById("modal-video");

        // 모달 창 닫기
        modal.style.display = "none";

        // 비디오 일시 정지
        modalVideo.pause();
    });

/*
// 비디오 재생
document.getElementById('view-video').addEventListener('click', () => {
    const selectedVideo = document.querySelector('#video-list li.clicked');

    if (selectedVideo) { 
        const videoIndex = parseInt(selectedVideo.getAttribute('video-index'));
        const videoToPlay = document.querySelector(`#spin-container video[video-index="${videoIndex}"]`);
        
        // 비디오를 전체 화면으로 변경
        if (videoToPlay.requestFullscreen) {
            videoToPlay.requestFullscreen();
            videoToPlay.play();

        // 아래는 다른 브라우저 환경
        } else if (videoToPlay.mozRequestFullScreen) {
            videoToPlay.mozRequestFullScreen();
        } else if (videoToPlay.webkitRequestFullscreen) {
            videoToPlay.webkitRequestFullscreen();
        } else if (videoToPlay.msRequestFullscreen) {
            videoToPlay.msRequestFullscreen();
        }
    } else {
        alert("재생할 비디오를 선택하세요.");
    }
});
*/

/*
// 선택 해제
document.getElementById("break").addEventListener("click", () => {
    // 선택된 비디오 요소 가져오기
    const selectedVideo = document.querySelector("#video-list li.clicked");

    if (selectedVideo) {
        // 선택된 비디오의 클릭 클래스 제거
        selectedVideo.classList.remove("clicked");

        // spin-container에서 선택된 모든 비디오의 'selected' 클래스 제거
        const allVideos = document.querySelectorAll("#spin-container video");
        allVideos.forEach((video) => video.classList.remove("selected"));
    } else {
        alert("해제할 비디오를 선택하세요.");
    }
});
*/

// 이미지 인덱스
let imageIndex = 1;

document.addEventListener("DOMContentLoaded", () => {
    const captureButton = document.querySelector(".capture-video");
    const videoElement = document.getElementById("modal-video");
    const imageList = document.getElementById("image-list");

    let capturedImages = [];

    captureButton.addEventListener("click", () => {
        const capturedImage = captureVideo(videoElement);
        capturedImages.push(capturedImage);
        updateImageList();
    });

    const captureVideo = (videoElement) => {
        const canvas = document.createElement("canvas");
        const context = canvas.getContext("2d");

        canvas.width = videoElement.videoWidth;
        canvas.height = videoElement.videoHeight;

        context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);

        const dataURL = canvas.toDataURL("image/png");

        return dataURL;
    };

    const updateImageList = () => {
        imageList.innerHTML = "";
        capturedImages.forEach((image, index) => {
            const listItem = document.createElement("li");
            listItem.textContent = `이미지 ${index + 1}`;
            listItem.addEventListener("click", () => {
                selectImage(index);
            });
            imageList.appendChild(listItem);
        });
    };

    const selectImage = (index) => {
        const imageItems = document.querySelectorAll("#image-list li");
        imageItems.forEach((item) => item.classList.remove("clicked"));
        imageItems[index].classList.add("clicked");
    };

    const viewImageButton = document.getElementById("view-image");
    const saveImageButton = document.getElementById("save-image");

    viewImageButton.addEventListener("click", () => {
        const selectedImageIndex = getSelectedImageIndex();
        if (selectedImageIndex !== -1) {
            const selectedImage = capturedImages[selectedImageIndex];
            viewImage(selectedImage);
        }
    });

    saveImageButton.addEventListener("click", () => {
        const selectedImageIndex = getSelectedImageIndex();
        if (selectedImageIndex !== -1) {
            const selectedImage = capturedImages[selectedImageIndex];
            saveImage(selectedImage);
        }
    });

    const getSelectedImageIndex = () => {
        const imageItems = document.querySelectorAll("#image-list li");
        for (let i = 0; i < imageItems.length; i++) {
            if (imageItems[i].classList.contains("clicked")) {
                return i;
            }
        }
        return -1;
    };

    const viewImage = (imageData) => {
        const imageViewer = window.open("", "_blank");
        imageViewer.document.write(
            `<img src="${imageData}" alt="Captured Image"/>`
        );
    };

    const saveImage = (imageData) => {
        const link = document.createElement("a");
        link.href = imageData;
        link.download = `이미지 ${imageIndex}`;
        link.click();
        imageIndex++; // 인덱스 증가
    };
});
