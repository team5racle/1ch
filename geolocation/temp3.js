// 애니메이션 시작과 함께 polyline 생성 및 거리 측정 함수
function drawAnimatedLine(startLatLng, endLatLng, map) {
    let linePath = [startLatLng]; // 시작 지점을 포함하는 경로 배열 초기화
    let polyline = new kakao.maps.Polyline({
        path: linePath,
        strokeWeight: 4,
        strokeColor: '#FF0000',
        strokeOpacity: 8,
        strokeStyle: 'dashed'
    });

    polyline.setMap(map); // polyline을 지도에 표시

    let interval = 20; // 간격 (밀리초)
    let step = 0.005; // 각 단계에서 이동할 경로의 비율
    let count = 0; // 현재 경로의 진행 상태를 추적

    let intervalId = setInterval(function() {
        count += step;
        if (count > 1) {
            count = 1; // 경로를 넘어서지 않도록 조정
        }

        let lat = startLatLng.getLat() + (endLatLng.getLat() - startLatLng.getLat()) * count;
        let lng = startLatLng.getLng() + (endLatLng.getLng() - startLatLng.getLng()) * count;
        let nextLatLng = new kakao.maps.LatLng(lat, lng);

        polyline.setPath([...polyline.getPath(), nextLatLng]); // 경로 배열에 새 위치 추가

        if(count ===1){
            clearInterval(intervalId); // 애니메이션 완료 후 인터벌 종료
            //polyline이 도착했을 때 거리 측정 및 출력
            let distance = Math.round(polyline.getLength());
            let message = '<div style="padding:5px;">현주님과의 거리는 ' + distance + '미터입니다.</div>'
            document.querySelector('#message').innerHTML = message;
        }

    }, interval);
}

function displayMarker(locPosition, title) {
    let imageSrc = 'marker4.png', // 마커 이미지 주소
        imageSize = new kakao.maps.Size(32, 34.5), 
        imageOption = {offset: new kakao.maps.Point(13.5, 34.5)};
    let markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
        marker = new kakao.maps.Marker({
            map: map,
            position: locPosition,
            image: markerImage
        });
    map.setCenter(locPosition);   

}

function displaySpecialMarker(locPosition, title) {
    let imageSrc_s = 'marker3.png', // 마커 이미지 주소
        imageSize_s = new kakao.maps.Size(32, 34.5), 
        imageOption_s = {offset: new kakao.maps.Point(13.5, 34.5)};

    let markerImage_s = new kakao.maps.MarkerImage(imageSrc_s, imageSize_s, imageOption_s),
        marker_s = new kakao.maps.Marker({
            map: map,
            position: locPosition,
            image: markerImage_s
        });
    map.setCenter(locPosition, title);   

    setTimeout(() => {
        // 마커에 'bounce' 클래스 추가
        let markers_s = document.querySelectorAll('img[src="' + imageSrc_s + '"]');
        markers_s.forEach((m) => {
            m.classList.add("bounce"); // 'className' 대신 'classList.add' 사용
        });
    
        // 커스텀 오버레이에 'bounce' 클래스 추가
        let overlay = document.querySelector('.customoverlay');
        overlay.classList.add("bounce");
    }, 0); // DOM이 로드된 후에 한 번에 모든 애니메이션 클래스 추가

}

// 지도 생성 및 초기화
let mapContainer = document.getElementById('map'), 
    mapOption = { 
        center: new kakao.maps.LatLng(33.450701, 126.570667),
        level: 8 
    }; 
let map = new kakao.maps.Map(mapContainer, mapOption);

let lat_o = 37.48293731084556, lon_o = 127.03607020985612; 
let locPosition_o = new kakao.maps.LatLng(lat_o, lon_o);

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        let lat_c = position.coords.latitude, 
            lon_c = position.coords.longitude; 
        let locPosition_c = new kakao.maps.LatLng(lat_c, lon_c);

        // 초기에 사용자와 '현주' 위치에 마커 표시
        displayMarker(locPosition_c, "사용자 위치");
        displaySpecialMarker(locPosition_o, "현주");

        // 버튼 클릭 이벤트 설정
        document.getElementById('startAnimation').addEventListener('click', function() {
            drawAnimatedLine(locPosition_c, locPosition_o, map);
        });
        }, function(error) {
            console.error(error);
        }, {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 1000
        });


} else { //위치 불러오기 실패시
    displaySpecialMarkerMarker(locPosition_o, "현주");
    let emessage = '<div style="padding:5px;">사용자의 위치를 불러올 수 없습니다...ㅠ</div>'
    document.querySelector('#message').innerHTML = emessage;
}



// 커스텀 오버레이에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
let content = '<div class="customoverlay bounce">' +
                '  <a href="https://map.kakao.com/link/map/11394059" target="_blank">' +
              '    <span class="title">현주</span>' +
                '  </a>' +
              '</div>';

// 커스텀 오버레이가 표시될 위치입니다 
let position = locPosition_o;  

// 커스텀 오버레이를 생성합니다
let customOverlay = new kakao.maps.CustomOverlay({
    map: map,
    position: position,
    content: content,
    yAnchor: 1 
});



// 10초 후에 애니메이션 종료
setTimeout(() => {
    document.querySelectorAll('.bounce').forEach((m) => {
        m.classList.remove("bounce"); // bounce 클래스 제거
    });
}, 10200); // 애니메이션이 시작된 후 10초 지난 시점