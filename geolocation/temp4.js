function panTo(locPosition) {
    // 이동할 위도 경도 위치를 생성합니다 
    let moveLatLon = locPosition;
    
    // 지도 중심을 부드럽게 이동시킵니다
    // 만약 이동할 거리가 지도 화면보다 크면 부드러운 효과 없이 이동합니다
    map.panTo(moveLatLon);            
}   

//거리에 따라 지도 레벨 조정하는 함수
function adjustMapLevel(startLatLng, endLatLng) {
    // 두 위치 사이의 거리 계산
    var poly = new kakao.maps.Polyline({
        path: [startLatLng, endLatLng]
    });
    var distance = poly.getLength(); // 미터 단위의 거리

    // 거리에 따른 지도 레벨 조정
    if (distance > 5000) { // 5km 이상
        map.setLevel(9);
    } else if (distance > 2000) { // 2km 이상 5km 미만
        map.setLevel(7);
    } else if (distance > 1000) { // 1km 이상 2km 미만
        map.setLevel(5);
    } else if (distance > 500) { // 500m 이상 1km 미만
        map.setLevel(3);
    } else { // 500m 미만
        map.setLevel(2);
    }

    // 출발지와 도착지 중앙으로 지도 중심 조정
    var bounds = new kakao.maps.LatLngBounds();
    bounds.extend(startLatLng);
    bounds.extend(endLatLng);
    map.setBounds(bounds);
}


// 애니메이션 시작과 함께 polyline 생성 및 거리 측정 함수
function drawAnimatedLine(startLatLng, endLatLng, map, title, color) {
    let linePath = [startLatLng]; // 시작 지점을 포함하는 경로 배열 초기화
    let polyline = new kakao.maps.Polyline({
        path: linePath,
        strokeWeight: 4,
        strokeColor: color,
        strokeOpacity: 8,
        strokeStyle: 'dashed'
    });

    polyline.setMap(map); // polyline을 지도에 표시

    let interval = 20; // 간격 (밀리초)
    let step = 0.005; // 각 단계에서 이동할 경로의 비율
    let count = 0; // 현재 경로의 진행 상태를 추적

    let intervalId = setInterval(function() {
        count += step;
        if (count > 1) {
            count = 1; // 경로를 넘어서지 않도록 조정
        }

        let lat = startLatLng.getLat() + (endLatLng.getLat() - startLatLng.getLat()) * count;
        let lng = startLatLng.getLng() + (endLatLng.getLng() - startLatLng.getLng()) * count;
        let nextLatLng = new kakao.maps.LatLng(lat, lng);

        polyline.setPath([...polyline.getPath(), nextLatLng]); // 경로 배열에 새 위치 추가

        if(count ===1){
            clearInterval(intervalId); // 애니메이션 완료 후 인터벌 종료
            //polyline이 도착했을 때 거리 측정 및 출력
            let distance = Math.round(polyline.getLength());
            let message = '<div style="padding:5px;">'+title+'님과의 거리는 ' + distance + '미터입니다.</div>'
            document.querySelector('#message').innerHTML = message;
        }

    }, interval);
}

function displayMarker(locPosition, title) {
    panTo(locPosition);
    let imageSrc = 'marker4.png', // 마커 이미지 주소
        imageSize = new kakao.maps.Size(32, 34.5), 
        imageOption = {offset: new kakao.maps.Point(13.5, 34.5)};
    let markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
        marker = new kakao.maps.Marker({
            map: map,
            position: locPosition,
            image: markerImage
        });
    // map.setCenter(locPosition); 
    
    // 커스텀 오버레이에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
    let content = '<div class="customoverlay">' +
    '  <a href="https://map.kakao.com/link/map/11394059" target="_blank">' +
    '    <span class="title">'+title+'</span>' +
    '  </a>' +
    '</div>';

    // 커스텀 오버레이를 생성합니다
    let customOverlay = new kakao.maps.CustomOverlay({
        map: map,
        position: locPosition,
        content: content,
        yAnchor: 1 
    });

}
// special: 사용자위치
function displaySpecialMarker(locPosition, title) {
    let imageSrc_s = 'marker3.png', // 마커 이미지 주소
        imageSize_s = new kakao.maps.Size(32, 34.5), 
        imageOption_s = {offset: new kakao.maps.Point(13.5, 34.5)};

    let markerImage_s = new kakao.maps.MarkerImage(imageSrc_s, imageSize_s, imageOption_s),
        marker_s = new kakao.maps.Marker({
            map: map,
            position: locPosition,
            image: markerImage_s
        });
    map.setCenter(locPosition, title);   

    setTimeout(() => {
        // 마커에 'bounce' 클래스 추가
        let markers_s = document.querySelectorAll('img[src="' + imageSrc_s + '"]');
        markers_s.forEach((m) => {
            m.classList.add("bounce"); // 'className' 대신 'classList.add' 사용
        });
    
        // 커스텀 오버레이에 'bounce' 클래스 추가
        // let overlay = document.querySelector('.customoverlay');
        // overlay.classList.add("bounce");
    }, 0); // DOM이 로드된 후에 한 번에 모든 애니메이션 클래스 추가

}

// 지도 생성 및 초기화
let mapContainer = document.getElementById('map'), 
    mapOption = { 
        center: new kakao.maps.LatLng(33.450701, 126.570667),
        level: 8 
    }; 
let map = new kakao.maps.Map(mapContainer, mapOption);

//37.559452239583116, 경도는 126.80206062130787
//위도는 37.5663781670156, 경도는 127.19415452892868 입니다
//위도는 37.5663781670156, 경도는 127.19415452892868 

//hj
let locPosition_hj = new kakao.maps.LatLng(37.48293731084556, 127.03607020985612);
//js
let locPosition_sh = new kakao.maps.LatLng(37.559452239583116, 126.80206062130787);
//jh
let locPosition_jh = new kakao.maps.LatLng(37.5663781670156, 127.19415452892868);
//sh
let locPosition_js = new kakao.maps.LatLng(34.88850314487177, 127.50318209628455);




if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        let lat_c = position.coords.latitude, 
            lon_c = position.coords.longitude; 
        let locPosition_c = new kakao.maps.LatLng(lat_c, lon_c);

        // 초기에 사용자 위치에 마커 표시
        displaySpecialMarker(locPosition_c, "사용자위치");

        // 버튼 클릭 이벤트 설정 hj
        document.getElementById('startAnimation_hj').addEventListener('click', function() {
            displayMarker(locPosition_hj, "현주");
            drawAnimatedLine(locPosition_c, locPosition_hj, map, "현주", '#FF0000');
            adjustMapLevel(locPosition_c, locPosition_hj); // 지도 레벨 조정
            document.querySelectorAll('.bounce').forEach((m) => {
                m.classList.remove("bounce"); // bounce 클래스 제거
            });
        });

        // 버튼 클릭 이벤트 설정 js
        document.getElementById('startAnimation_js').addEventListener('click', function() {
            displayMarker(locPosition_js, "준석");
            drawAnimatedLine(locPosition_c, locPosition_js, map, "준석", '#75B8FA');
            adjustMapLevel(locPosition_c, locPosition_js);
            document.querySelectorAll('.bounce').forEach((m) => {
                m.classList.remove("bounce"); // bounce 클래스 제거
            });
        });
        // 버튼 클릭 이벤트 설정
        document.getElementById('startAnimation_jh').addEventListener('click', function() {
            displayMarker(locPosition_jh, "재현");
            drawAnimatedLine(locPosition_c, locPosition_jh, map, "재현", '#152611');
            adjustMapLevel(locPosition_c, locPosition_jh);
            document.querySelectorAll('.bounce').forEach((m) => {
                m.classList.remove("bounce"); // bounce 클래스 제거
            });
        });
        // 버튼 클릭 이벤트 설정
        document.getElementById('startAnimation_sh').addEventListener('click', function() {
            displayMarker(locPosition_sh, "상훈");
            drawAnimatedLine(locPosition_c, locPosition_sh, map, "상훈", '#FF3DE5');
            adjustMapLevel(locPosition_c, locPosition_sh);
            document.querySelectorAll('.bounce').forEach((m) => {
                m.classList.remove("bounce"); // bounce 클래스 제거
            });
        });





        
        }, function(error) {
            console.error(error);
        }, {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 1000
        });


} else { //위치 불러오기 실패시
    let emessage = '<div style="padding:5px;">사용자의 위치를 불러올 수 없습니다...ㅠ</div>'
    document.querySelector('#message').innerHTML = emessage;
}









