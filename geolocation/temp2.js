function drawAnimatedLine(startLatLng, endLatLng, map) {
    let linePath = [startLatLng]; // 시작 지점을 포함하는 경로 배열 초기화
    let polyline = new kakao.maps.Polyline({
        path: linePath,
        strokeWeight: 3,
        strokeColor: '#FF0000',
        strokeOpacity: 0.5,
        strokeStyle: 'solid'
    });

    polyline.setMap(map); // polyline을 지도에 표시

    let interval = 20; // 간격 (밀리초)
    let step = 0.005; // 각 단계에서 이동할 경로의 비율
    let count = 0; // 현재 경로의 진행 상태를 추적

    let intervalId = setInterval(function() {
        count += step;
        if (count > 1) {
            count = 1; // 경로를 넘어서지 않도록 조정
            clearInterval(intervalId); // 애니메이션 완료 후 인터벌 종료
        }

        let lat = startLatLng.getLat() + (endLatLng.getLat() - startLatLng.getLat()) * count;
        let lng = startLatLng.getLng() + (endLatLng.getLng() - startLatLng.getLng()) * count;
        let nextLatLng = new kakao.maps.LatLng(lat, lng);

        polyline.setPath([...polyline.getPath(), nextLatLng]); // 경로 배열에 새 위치 추가
    }, interval);
}



let mapContainer = document.getElementById('map'), 
    mapOption = { 
        center: new kakao.maps.LatLng(33.450701, 126.570667),
        level: 8 
    }; 
let map = new kakao.maps.Map(mapContainer, mapOption);

let lat1 = 37.48293731084556, lon1 = 127.03607020985612; 
let locPosition1 = new kakao.maps.LatLng(lat1, lon1);
displayMarker(locPosition1);

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        let lat = position.coords.latitude, 
            lon = position.coords.longitude; 
        let locPosition = new kakao.maps.LatLng(lat, lon);

        drawAnimatedLine(locPosition, locPosition1, map); // 애니메이션을 시작하는 함수 호출

        // 여기에서 linePath를 정의합니다.
        let linePath2 = [
            new kakao.maps.LatLng(lat1, lon1),
            new kakao.maps.LatLng(lat, lon)
        ];

        // 거리를 구하기 위한 polyline
        let polyline2 = new kakao.maps.Polyline({
            path: linePath2,
            strokeWeight: 3,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeStyle: 'solid'
        });
        let distance = Math.round(polyline2.getLength());
        // alert('선의 거리는 ' + distance + '미터입니다.');
        let message = '<div style="padding:5px;">현주님과의 거리는 ' + distance + '미터입니다.</div>'
        
        displayMarker(locPosition);
        document.querySelector('#message').innerHTML = message;
    }, function(error) {
        console.error(error);
    }, {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
    });
} else { //위치 불러오기 실패시
    let locPosition = new kakao.maps.LatLng(33.450701, 126.570667),    
        message = 'geolocation을 사용할수 없어요..';
    displayMarker(locPosition, message);
}

function displayMarker(locPosition) {
    let imageSrc = 'marker3.png', // 마커이미지의 주소입니다    
    imageSize = new kakao.maps.Size(32, 34.5), // 마커이미지의 크기입니다
    imageOption = {offset: new kakao.maps.Point(13.5, 34.5)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
    let markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption)
    let marker = new kakao.maps.Marker({  
        map: map, 
        position: locPosition,
        image: markerImage // 마커이미지 설정 
    }); 
    // let infowindow = new kakao.maps.InfoWindow({
    //     content: message,
    //     removable: true
    // });
    // infowindow.open(map, marker);
    map.setCenter(locPosition);      
}

// 커스텀 오버레이에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
let content = '<div class="customoverlay">' +
                '  <a href="https://map.kakao.com/link/map/11394059" target="_blank">' +
              '    <span class="title">현주</span>' +
                '  </a>' +
              '</div>';

// 커스텀 오버레이가 표시될 위치입니다 
let position = locPosition1;  

// 커스텀 오버레이를 생성합니다
let customOverlay = new kakao.maps.CustomOverlay({
    map: map,
    position: position,
    content: content,
    yAnchor: 1 
});
