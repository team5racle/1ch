let mapContainer = document.getElementById('map'), 
    mapOption = { 
        center: new kakao.maps.LatLng(33.450701, 126.570667),
        level: 10 
    }; 
let map = new kakao.maps.Map(mapContainer, mapOption);

let lat1 = 37.48293731084556, lon1 = 127.03607020985612; 
let locPosition1 = new kakao.maps.LatLng(lat1, lon1);
let message1 = '<div style="padding:5px;">현주님</div>'
displayMarker1(locPosition1, message1);

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        let lat = position.coords.latitude, 
            lon = position.coords.longitude; 
        let locPosition = new kakao.maps.LatLng(lat, lon);
             

        // 여기에서 linePath를 정의합니다.
        let linePath = [
            new kakao.maps.LatLng(lat1, lon1),
            new kakao.maps.LatLng(lat, lon)
        ];

        // polyline 객체를 여기에서 생성합니다.
        let polyline = new kakao.maps.Polyline({
            path: linePath,
            strokeWeight: 3,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeStyle: 'solid'
        });

        polyline.setMap(map);
        let distance = Math.round(polyline.getLength());
        // alert('선의 거리는 ' + distance + '미터입니다.');
        let message = '<div style="padding:5px;">현주님과의 거리는 ' + distance + '미터입니다.</div>'
        
        displayMarker(locPosition, message);

    }, function(error) {
        console.error(error);
    }, {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
    });
} else {
    let locPosition = new kakao.maps.LatLng(33.450701, 126.570667),    
        message = 'geolocation을 사용할수 없어요..';
    displayMarker(locPosition, message);
}

function displayMarker1(locPosition, message) {
    let imageSrc = 'marker1.png', // 마커이미지의 주소입니다    
    imageSize = new kakao.maps.Size(32, 34.5), // 마커이미지의 크기입니다
    imageOption = {offset: new kakao.maps.Point(13.5, 34.5)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
    let markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption)
    let marker = new kakao.maps.Marker({  
        map: map, 
        position: locPosition,
        image: markerImage // 마커이미지 설정 
    }); 
    let infowindow = new kakao.maps.InfoWindow({
        content: message,
        removable: true
    });
    infowindow.open(map, marker);
    map.setCenter(locPosition);      
}
function displayMarker(locPosition, message) {
    let imageSrc = 'marker1.png', // 마커이미지의 주소입니다    
    imageSize = new kakao.maps.Size(32, 34.5), // 마커이미지의 크기입니다
    imageOption = {offset: new kakao.maps.Point(13.5, 34.5)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
    let markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption)
    let marker = new kakao.maps.Marker({  
        map: map, 
        position: locPosition,
        image: markerImage // 마커이미지 설정 
    }); 
    let infowindow = new kakao.maps.InfoWindow({
        content: message,
        removable: true
    });
    infowindow.open(map, marker);
    map.setCenter(locPosition);      
}
